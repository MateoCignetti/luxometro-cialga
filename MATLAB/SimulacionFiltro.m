inputCoefficients = [0.01165, 0.06992, 0.1748, 0.2331, 0.1748, 0.06992, 0.01165];
outputCoefficients = [1, 1.063, -1.201, 0.5836, -0.2318, 0.04371, -0.004295];

outputArray = zeros(1,7);
inputArray = zeros(1,7);

t = 0 : 0.005 : pi;
signal = sin(5*2*pi*t);
noise = 0.1*rand(1, length(t));
inputSignal = signal + noise;

figure(1)
hold on
stairs(inputSignal, 'blue')

outputSignal = [];

for w = 1:length(t)
    inputArray(1) = inputSignal(w);
    outputSum = inputCoefficients(1)*inputArray(1);
    for u = 2:7
        outputSum = outputSum + inputCoefficients(u) * inputArray(u) + outputCoefficients(u) * outputArray(u);
    end
    outputArray(1) = outputSum;
    outputSignal(w) = outputArray(1);

    for v = 7:-1:2
        outputArray(v) = outputArray(v-1);
        inputArray(v) = inputArray(v-1);
    end
end
xlim([0 100]);
grid on;
stairs(outputSignal,'red');
legend('Señal de entrada','Señal de salida');
title("Simulación del filtro en Matlab");

