fp = 40;
fs = 50;

Rp = 3;
Rs = 15;

fmax = 100
fmuestreo = 2*fmax;

Tm = 1/fmuestreo;

wp = fp/fmax;
ws = fs/fmax;

[N, Wn] = buttord(wp, ws, Rp, Rs)
[b, a] = butter(N, Wn,'low') 
filtro_butter = filt(b,a,Tm)
bode(filtro_butter)
grid on
title("Diagrama de Bode")

% t = 0 : 0.005 : pi;
% signal = sin(5*2*pi*t);
% noise = 0.1*rand(1, length(t));
% 
% inputSignal = signal + noise;
% 
% lsim(filtro_butter, inputSignal)